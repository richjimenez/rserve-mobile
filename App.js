import React, { useEffect, useState } from 'react';
import { Notifications } from 'expo';
import { AppearanceProvider } from 'react-native-appearance';
import * as Permissions from 'expo-permissions';
import * as TaskManager from 'expo-task-manager';
import * as Location from 'expo-location';

import { ThemeProvider } from 'react-native-elements';

// fix atob bug on firebase sdk
import { decode, encode } from 'base-64';
if (!global.btoa) global.btoa = encode;
if (!global.atob) global.atob = decode;

// Navigation
import { NavigationContainer } from '@react-navigation/native';
import Routes from './Routes';

// redux
import { Provider, useDispatch } from 'react-redux';
import store from './redux/store';
import { setCurrentLocation } from './redux/locations/actions';

const LOCATION_GEOFENCING = 'background-geofencing-task';

// 20.629825, -103.422825
const regions = [
  {
    identifier: 'Restaurante 1',
    latitude: 20.626256,
    longitude: -103.422407,
    radius: 50,
  },
];

const handleNotification = notification => {
  console.log(notification);
  console.log('ok! got your notif');
};

const showNotification = (title, body) => {
  const notification = {
    title,
    body,
    data: { title, body },
    ios: { sound: true, _displayInForeground: true },
  };
  const schedulingOptions = { time: new Date().getTime() + 2000 };
  Notifications.scheduleLocalNotificationAsync(notification, schedulingOptions);
};

const theme = {
  colors: {
    primary: 'red',
  },
  Button: {
    raised: true,
    titleStyle: {
      color: 'white',
    },
  },
};

export default () => {
  const [token, setToken] = useState('nein');

  const askNotification = async () => {
    // We need to ask for Notification permissions for ios devices
    const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
    if (status === 'granted') {
      console.log('Notification granted');
    }
    const token = await Notifications.getExpoPushTokenAsync();
    const deviceToken = await Notifications.getDevicePushTokenAsync({ gcmSenderId: '1033962785393' });
    console.log(token);
    console.log(deviceToken);
    setToken(deviceToken);
  };

  const askLocation = async () => {
    console.log(await Location.getProviderStatusAsync());
    const { backgroundModeEnabled } = await Location.getProviderStatusAsync();
    const { status } = await Location.requestPermissionsAsync();
    if (status === 'granted') {
      console.log('Location granted.');
      if (backgroundModeEnabled) await Location.startGeofencingAsync(LOCATION_GEOFENCING, regions);
      // const options = { accuracy: Location.Accuracy.BestForNavigation, timeInterval: 2000 };
      // await Location.watchPositionAsync(options, data => {
      //   console.log('Current location:', data.coords);
      // });
    }
  };

  useEffect(() => {
    askLocation();
    // askNotification();
    // If we want to do something with the notification when the app
    // is active, we need to listen to notification events and
    // handle them in a callback
    const listener = Notifications.addListener(handleNotification);
    return () => listener.remove();
  }, []);

  return (
    <Provider store={store}>
      <AppearanceProvider>
        <NavigationContainer>
          <ThemeProvider theme={theme}>
            <Routes />
          </ThemeProvider>
        </NavigationContainer>
      </AppearanceProvider>
    </Provider>
  );
};

// background tasks
TaskManager.defineTask(LOCATION_GEOFENCING, ({ data: { eventType, region }, error }) => {
  if (eventType === Location.GeofencingRegionState.Inside) {
    store.dispatch(setCurrentLocation(region));
    showNotification(`LLegaste a: ${region.identifier}`, 'Bienvenido!');
  } else {
    store.dispatch(setCurrentLocation(null));
  }
  // if (eventType === Location.GeofencingEventType.Enter) {
  //   showNotification(`LLegaste a: ${region.identifier}`, 'Bienvenido madafaker');
  //   console.log("You've entered to:", region.identifier);
  // } else if (eventType === Location.GeofencingEventType.Exit) {
  //   showNotification(`Saliste de: ${region.identifier}`, 'Y no regreses madafaker!');
  //   console.log("You've left:", region.identifier);
  // } else {
  //   console.log(error);
  // }
});
