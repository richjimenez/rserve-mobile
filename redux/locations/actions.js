import locationsTypes from './types';

export const setCurrentLocation = location => {
  return {
    type: locationsTypes.SET_CURRENT_LOCATION,
    payload: location,
  };
};
