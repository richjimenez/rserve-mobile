import locationsTypes from './types';

const initState = { currentLocation: null, locations: [] };

const locationsReducer = (state = initState, { type, payload }) => {
  switch (type) {
    case locationsTypes.SET_CURRENT_LOCATION:
      return {
        ...state,
        currentLocation: payload,
      };

    default:
      return state;
  }
};

export default locationsReducer;
