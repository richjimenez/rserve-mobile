import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { View, Text, StyleSheet, Dimensions } from 'react-native';
import { Button } from 'react-native-elements';
import MapView from 'react-native-maps';
import Constants from 'expo-constants';
import { auth } from './firebase';

import { useColorScheme } from 'react-native-appearance';

const Home = props => {
  const currentLocation = useSelector(state => state.locations.currentLocation);

  const colorScheme = useColorScheme();
  console.log(colorScheme);

  const signOut = () => {
    auth
      .signOut()
      .then(function() {
        console.log('Sign-out successful.');
      })
      .catch(function(error) {
        // An error happened.
      });
  };

  return (
    // <View style={styles.container}>
    //   {currentLocation ? (
    //     <Text style={styles.paragraph}>{currentLocation.identifier}</Text>
    //   ) : (
    //     <Text style={styles.paragraph}>No estas cerca de ninguna de nuestras ubicaciones</Text>
    //   )}
    // </View>
    <View style={styles2.container}>
      <MapView
        style={styles2.mapStyle}
        showsUserLocation
        initialRegion={{
          latitude: 20.626256,
          longitude: -103.422407,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421,
        }}
      >
        {/* <Marker coordinate={marker.latlng} title={marker.title} description={marker.description} /> */}
      </MapView>
      {currentLocation ? (
        <Text style={styles.paragraph}>{currentLocation.identifier}</Text>
      ) : (
        <Text style={styles.paragraph}>No estas cerca de ninguna de nuestras ubicaciones</Text>
      )}
      <Text style={styles.paragraph}>Token: </Text>
      <Button title="Go to Details" onPress={() => props.navigation.navigate('Details')} />
      <Button title="Salir" onPress={signOut} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
    backgroundColor: 'black',
  },
  paragraph: {
    margin: 24,
    fontSize: 18,
    textAlign: 'center',
    color: 'white',
  },
});

const styles2 = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    //justifyContent: 'center',
    backgroundColor: 'black',
  },
  mapStyle: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height - 400,
  },
});

export default Home;
