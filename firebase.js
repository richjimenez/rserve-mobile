import * as firebase from 'firebase/app';

import 'firebase/auth';
import 'firebase/firestore';

const firebaseConfig = {
  apiKey: 'AIzaSyBDha8WCfb1qYhwqXtNWZhiUFJaYkEGNZI',
  authDomain: 'rserve-37e96.firebaseapp.com',
  databaseURL: 'https://rserve-37e96.firebaseio.com',
  projectId: 'rserve-37e96',
  storageBucket: 'rserve-37e96.appspot.com',
  messagingSenderId: '1033962785393',
  appId: '1:1033962785393:web:a094ba85ea259f162429d0',
};

firebase.initializeApp(firebaseConfig);

export const auth = firebase.auth();
export const db = firebase.firestore();

export default firebase;
