import React, { useState, useEffect } from 'react';
import { View, Text } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { useDispatch, useSelector } from 'react-redux';
import { setCurrentUser } from './redux/auth/actions';

// firebase
import { auth } from './firebase';

// Screens
import Home from './Home';
import SignIn from './SignIn';

const Stack = createStackNavigator();

function DetailsScreen() {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Details Screen</Text>
    </View>
  );
}

function SplashScreen() {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Loading</Text>
    </View>
  );
}

export default () => {
  const dispatch = useDispatch();
  const [user, setUser] = useState(null);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const unlisten = auth.onAuthStateChanged(user => {
      console.log('onAuthChange', user);
      setUser(user);
      dispatch(setCurrentUser(user));
      setIsLoading(false);
    });
    return () => unlisten();
  }, []);

  if (isLoading) return <SplashScreen />;

  return (
    <Stack.Navigator>
      {user === null ? (
        <Stack.Screen
          name="SignIn"
          component={SignIn}
          options={{
            title: 'Sign in',
            animationTypeForReplace: 'pop',
          }}
        />
      ) : (
        // User is signed in
        <>
          <Stack.Screen name="Home" component={Home} options={{ title: 'Overview' }} />
          <Stack.Screen name="Details" component={DetailsScreen} />
        </>
      )}
    </Stack.Navigator>
  );
};
