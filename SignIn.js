import React, { useState, useRef } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/EvilIcons';
import { Input, Button } from 'react-native-elements';
import Constants from 'expo-constants';
import { auth } from './firebase';

const SignIn = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const passwordRef = useRef(null);

  const handleSubmit = () => {
    console.log(email, password);
    auth.signInWithEmailAndPassword(email, password).catch(error => {
      console.log(error.code);
      console.log(error.message);
    });
  };

  return (
    <View style={styles.container}>
      <Input
        placeholder="Correo electronico"
        onChangeText={text => setEmail(text)}
        value={email}
        textContentType="username"
        autoCapitalize="none"
        returnKeyType="next"
        onSubmitEditing={() => passwordRef.current.focus()}
        leftIcon={{ name: 'envelope', type: 'evilicon', color: 'red' }}
      />
      <Input
        placeholder="Contraseña"
        ref={passwordRef}
        onChangeText={text => setPassword(text)}
        value={password}
        secureTextEntry
        textContentType="password"
        autoCapitalize="none"
        returnKeyType="send"
        leftIcon={{ name: 'lock', type: 'evilicon', color: 'red' }}
        onSubmitEditing={handleSubmit}
        style={styles.password}
      />
      <Button style={styles.btn} title="Iniciar sesion" onPress={handleSubmit} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'lightgrey',
    //paddingTop: Constants.statusBarHeight,
  },
  password: {
    marginBottom: 50,
  },
  btn: {},
});

export default SignIn;
